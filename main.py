from random import randint
from os import path
from PIL import Image, ImageTk
import tkinter


class Game:
    def __init__(self):
        self.words = [
            "javascript",
            "python",
            "java",
            "html"
        ]
        self.current_word = self.words[randint(0, len(self.words) - 1)]
        self.used_letters = []

        self.root = tkinter.Tk()

        self.hidden_word = tkinter.StringVar(value="_ " * len(self.current_word))
        self.lives = tkinter.IntVar(value=5)

        self.label1 = tkinter.Label(self.root, text="choisissez une lettre avec votre clavier")
        self.label1.grid()

        self.label2 = tkinter.Label(self.root, textvariable=self.lives)
        self.label2.grid()

        self.label3 = tkinter.Label(self.root, textvariable=self.hidden_word)
        self.label3.grid()

        self.root.bind("<KeyPress>", self.handle_key_press)

        self.root.mainloop()

    def handle_key_press(self, event):
        # print(event.__dict__)
        print(event.char)
        if event.char not in self.used_letters:
            self.used_letters.append(event.char)
            if event.char in self.current_word:
                word = ""
                for letter in self.current_word:
                    if letter in self.used_letters:
                        word += letter + " "
                    else:
                        word += "_ "
                self.hidden_word.set(word)
            else:
                self.lives.set(self.lives.get() - 1)
                if self.lives.get() == 0:
                    self.loose()

    def loose(self):
        self.label1.destroy()
        self.label2.destroy()
        self.label3.destroy()

        img_path = path.join("assets", "loose.png")
        img_file = Image.open(img_path)

        img_file = img_file.resize((50, 50))

        img_tk = ImageTk.PhotoImage(img_file)

        loose_label = tkinter.Label(self.root)
        loose_label.config(image=img_tk)
        loose_label.grid()
        loose_label.image = img_tk

game = Game()
